## Task 1 -- Goal

The aim of the session is to create a fake Instagram page, as if you were a
famous mathematician. The page must contain
+ a short biography, in the Instagram style (around 4 lines)
+ 3 or 4 relevant hashtags
+ a profile picture
+ a username 
+ 6 posts, each with
    + an illustration or photo
    + some text
    + hashtags

---

Some mathematicians that you may choose:

+ [Charles Babbage](https://en.wikipedia.org/wiki/Charles_Babbage)
+ [Émilie du
  Châtelet](https://en.wikipedia.org/wiki/%C3%89milie_du_Ch%C3%A2telet)
+ [Leonhard Euler](https://en.wikipedia.org/wiki/Leonhard_Euler)
+ [Carl Friedrich Gauss](https://en.wikipedia.org/wiki/Carl_Friedrich_Gauss)
+ [Sophie Germain](https://en.wikipedia.org/wiki/Sophie_Germain)
+ [Katherine Johnson](https://en.wikipedia.org/wiki/Katherine_Johnson)
+ [Ada Lovelace](https://en.wikipedia.org/wiki/Ada_Lovelace)
+ [Maryam Mirzakhani](https://en.wikipedia.org/wiki/Maryam_Mirzakhani)
+ [John Napier](https://en.wikipedia.org/wiki/John_Napier)
+ [Isaac Newton](https://en.wikipedia.org/wiki/Isaac_Newton)
+ [Emmy Noether](https://en.wikipedia.org/wiki/Emmy_Noether)
+ [James Stirling](https://en.wikipedia.org/wiki/James_Stirling)
+ [Alan Turing](https://en.wikipedia.org/wiki/Alan_Turing)
+ [Karen Uhlenbeck](https://en.wikipedia.org/wiki/Karen_Uhlenbeck)
+ [Maryna Viazovska](https://en.wikipedia.org/wiki/Maryna_Viazovska)
+ [John Wallis](https://en.wikipedia.org/wiki/John_Wallis)
+ [Andrew Wiles](https://en.wikipedia.org/wiki/Andrew_Wiles)

---

## An example

Here is an example with the mathematician Gauss.

![An example with Gauss](images/example-insta.jpg)

---

## Task 2: interview

After you are done with the Instagram pages, you will have to present your work
to your classmates. The presentation will take the form of an interview.

**More instructions:**
+ the interview must last (at least) 3 minutes ;
+ you must display the instagram profile of the mathematician you chose ;
+ one of you should impersonate the mathematician and the other comment on
  his/her life and Instagram profile, as his/her friend/relative/fan/*etc*. 
