<div style="text-align:center">
<h2> TDNL Maths </h2>
<p> The website of your mathematics class. </p>
</div>

---

## <i class="fa fa-cogs" aria-hidden="true"></i> User manual

<p>
The icon
<i class="fa fa-align-justify"></i>
allows you to show or hide the navigation menu. In that menu, you will
find ressources linked with our mathematics class.
</p>
<p>
The arrows
<i class="fa fa-angle-left"></i>
and
<i class="fa fa-angle-right"></i>
allow you to navigate through the website. On a computer, you can also use
your keyboard.
</p>
<p>
The icon
<i class="fa fa-font"></i>
is used to change the font, its size, and the website theme (light, dark, or
sepia).
</p>
<p>
Finally, you can use the search bar to find a key-word throughout all the
website.
</p>

## <i class="fa fa-envelope" aria-hidden="true"></i> How to contact me

The easiest way to contact me is to use the ENT and search for my name. If
this solution does not work for you for some reason, you can also reach me
through my email address: edouard.rousseau@ac-versailles.fr.

