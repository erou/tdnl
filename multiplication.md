## How do you multiply?

### A short film

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/Nc4yrFXw20Q?si=01rM_m-Q2EFid5H3" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>

**Some questions:**
+ What are the main themes of the film?
+ What places are mentionned?
+ What is the link between this multiplication technique and computers?

---

### Ethiopian multiplication

*How?*

1. Compute $$13\times 24$$ using [Ethiopian multiplication](https://en.wikipedia.org/wiki/Ancient_Egyptian_multiplication).
2. Try with $$53\times 32$$.

*Why?*

Notice that the number $$137$$ means $$1\times 10^2+3\times10^1+7\times10^0$$,
*i.e.* we decompose the number $$137$$ into powers of $$10$$.

+ Long multiplication uses the decimal decomposition of a number, also called
  *base 10*.
+ Ethiopian multiplication decomposes a number in powers of $$2$$, also called
  *base 2*.
    + Indeed, we use $$25 = 16 + 8 + 1$$.
    + Then $$25\times 31$$ can be written $$16\times31+8\times31+1\times31$$.
+ Write $$25 = 2\times 12+1 = 2\times (2\times 6) + 1 = \ldots = 16+8+1$$

*How to find the binary representation of a number?*

+ Play with the cards
