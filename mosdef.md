## A song about mathematics?

[Yasiin Bey](https://en.wikipedia.org/wiki/Mos_Def) is an American rapper and
activist. He is more commonly known by his former stage name **Mos Def**.

*Mathematics* is a song from 1999, written by Mos Def.

<div style="text-align:center">
<audio controls>
  <!--<source src="horse.ogg" type="audio/ogg">-->
  <source src="audio/mathematics-mos-def.mp3" type="audio/mpeg">
Your browser does not support the audio element.
</audio>
</div>

The lyrics of the song can be found, for example,
[here](https://genius.com/Yasiin-bey-mathematics-lyrics).

### Some questions 

+ What are the references to
    + numbers? 
    + Mathematical concepts?
    + Statistics?
+ *(With the lyrics only)* When was the song written?
+ *(With the lyrics only)* What musical style do you think it is?
+ What is the song about?
    + What does the author criticize?
    + What is the end of the song suggesting?
    + According to the author, how do young Black people fight poverty? What are
      the risks?
+ Why is the author involving mathematics to deal with this subject?
