# Summary

* [Home](README.md)

## History of mathematics
* [Emmy Noether](noether.md)
* [Mathematicians' Instagram](insta.md)

## Statistics
* [*Mathematics*, by Mos Def](mosdef.md)
* [2021 United Kingdom census](ukcensus.md)
* [Simpson's paradox](simpson.md)
* [Toolbox](toolbox.md)
* [Final task](stat-project.md)

## Calculus
* [How do you multiply?](multiplication.md)
* [Introduction to calculus](intro-calculus.md)
