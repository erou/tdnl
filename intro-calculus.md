## Introduction to calculus

### A short film

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/rBVi_9qAKTU?si=Vmr0CJ5rNjDnvLKK" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>

**Some questions:**

1. What method did the Ancient Greeks use in order to compute the area of a
  curved shape?
2. According to Professor Dave, this method has inspired another kind of
  calculation. What is it?
3. Who was the first mathematician to make considerable progress in this
  field?
4. Why did Newton develop calculus? For which other field did he need to
  use these kinds of calculations?
5. What did Newton put in the spotlight regarding falling objects and
  velocity?
6. List the two types of calculus mentioned by Professor Dave.

---

### Other videos

We will not have time to study his work, but Grant Sanderson, also known as
[3Blue1Brown](https://www.youtube.com/@3blue1brown) on YouTube, made *very*
interesting videos about calculus.

Here is the first of the series.

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/WUvTyaaNkzM?si=d-mIgZBXJRZcTEAI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>

He also made other videos on many different mathematical subjects. Calculus is
one of the most represented subject on the Internet, due to its applications in
engineering.
