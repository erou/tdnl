## Statistics: final task

---

### Instructions

- Choose a(ny) subject
    - Preferrably something happening in an English-speaking country
- Make an A4/A3 poster in order to
    - understand something
    - convince about something
- You poster should contain a description to clarify what you want to present
- You can also add images
- You **must** use statistics in your poster, you should use at least three
  different statistical tools among
    - Scatter plots
    - Best fit lines
    - Correlation factors
    - Box plots
    - Means / Medians
    - Standard deviations / Variances
- You should also **explain** how to interpret the statistical tools you use
- In order to obtain data you can use
    - The website of [the United States Census Bureau](https://www.census.gov/),
      which contains data on the US
    - The website of [the Office for National Statistics](https://www.ons.gov.uk/),
      which contains data on the UK
    - [Our World in Data](https://ourworldindata.org/)
    - Any other website with data on the specific subject you want to address

---

### Examples

- [Deforestation](https://en.wikipedia.org/wiki/Deforestation)
- [Women in Science, Technology, Engineering and
  Mathematics](https://www.aauw.org/resources/research/the-stem-gap/) (STEM)
- *etc.*

---

### Scale

- Statistical tools (4 points): relevance, correctness, diversity, explanations,
  readability, quality of the sources
- Description of the subject (2 points): clear, understandable, link with the
  English-speaking world
- Impact (2 points): convincing, clear
- Language (2 points): correctness, syntax
