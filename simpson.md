## Simpson's paradox

### An easy answer?

In a neighborhood of London, highschool students pass their A-levels. In 2023,
1446 women obtained the A-level, whereas 1368 men obtained theirs.

**Question 1a.** Which group performed best, men or women?

**Question 1b.** Does this make sense?

We now know that they were 1647 women who took the examination. There were also
1542 men.

**Question 2.** Does this change the answer? Why?

Finally, we obtain even more information. Many subjects can be studied
in order to obtain the A-level. In order to simplify, let us consider that
students fall into one of the categories "Humanities", "Economics" and
"Sciences".

- In Humanities
    - 321 women obtained their A-level out of 419.
    - 187 men obtained their A-level out of 250.
- In Economics 
    - 458 women obtained their A-level out of 521.
    - 393 men obtained their A-level out of 451.
- In Sciences
    - 667 women obtained their A-level out of 707.
    - 788 men obtained their A-level out of 841.

**Question 3.** Who performed best in Humanities? Economics? Sciences?

**Question 4.** Try to explain why the result is different when we look at the numbers
    without knowledge of the subjects studied.

### An explanation

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/ebEkn-BiW5k?si=W57va-PXuBzsij1j" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>
