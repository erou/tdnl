## A statistician's toolbox

### <i class="fa fa-pencil" aria-hidden="true"></i> Vocabulary

+ *census:* recensement
+ *average:* moyenne
+ *mean:* moyenne
+ *median:* médiane
+ *standard deviation:* écart-type
+ *variance:* variance
+ *box plot:* boîte à moustaches
+ *scatter plot:* nuage de points

### Formulas

Let $$x_1, x_2, ... , x_{n-1}, x_n\in\mathbb{R}$$ be real numbers.

+ The *mean* is often denoted by $$\overline x$$ and is given by 

<div style="text-align:center">
$${\displaystyle \overline x = \frac{1}{n}\times\sum_{k=1}^n x_k.}$$
</div>

+ The *variance* measures the dispersion of the values $$x_k$$. It is denoted by
the the letter $$V$$ and is given by

<div style="text-align:center">
$${\displaystyle V = \frac{1}{n}\times\sum_{k=1}^n (\overline x -x_k)^2.}$$
</div>

+ The *standard deviation* is denoted by the Greek letter $$\sigma$$ and is
  given by

<div style="text-align:center">
$${\displaystyle \sigma = \sqrt{V}.}$$
</div>

Let $$y_1, y_2, ... , y_{n-1}, y_n\in\mathbb{R}$$ be real numbers. We now have
two samples of data $$x$$ and $$y$$.

+ We can compute their *covariance*

<div style="text-align:center">
$${\displaystyle \mathrm{Cov}(x,y) = \frac{1}{n}\sum_{k=1}^n (\overline x-x_k)(\overline
y-y_k).}$$
</div>

If the respective standard deviations of the values $$x$$ and $$y$$ are denoted by
$$\sigma_x$$ and $$\sigma_y$$, we then obtain their

+ *Pearson correlation coefficient*, denoted by $$r$$, and given by

<div style="text-align:center">
$${\displaystyle r = \frac{\mathrm{Cov}(x,y)}{\sigma_x\sigma_y}.}$$
</div>

We always have $$-1\leq r \leq 1$$. If the values $$x$$ and $$y$$ are mostly independant,
we obtain $$r\approx0$$, whereas a value $$r\approx\pm1$$ means that the values
$$x$$ and $$y$$ are correlated.

### Application

Three students, Alice, Beatriz, and Charles, are in the same class. Out of
guilt, they admitted that they cheated during their year together. By analyzing
their marks, can you understand who cheated?

Alice | Beatriz | Charles  
:----:|:-------:|:------:
12|2|15
15|12|12
3|12|6
12|6|16
3|5|6
18|17|18
8|5|9
8|16|11
4|9|0
7|13|14
9|13|12
8|14|2
12|10|14
17|17|12
16|9|9
14|5|12
9|9|16
16|10|9
16|9|8
1|15|7

### Correlations

Sometimes two things can be correlated (have $$r\approx1$$), just by
[coincidence](https://www.tylervigen.com/spurious-correlations).
