## A great mind: Emmy Noether

[Emmy Noether](https://en.wikipedia.org/wiki/Emmy_Noether) was a German
mathematician. The following audio document is the introduction of a podcast
about her, the full version can be found on the [BBC
website](https://www.bbc.co.uk/sounds/play/m00025bw).

<div style="text-align:center">
<audio controls>
  <!--<source src="horse.ogg" type="audio/ogg">-->
  <source src="audio/noether.mp3" type="audio/mpeg">
Your browser does not support the audio element.
</audio>
</div>

<button onclick="hide()">See the transcription</button>

<blockquote id="transcription" style="display:none">
Hello, Emmy Noether was one of the great innovative mathematicians of the 20th century, and her ideas have underpined much in modern physics and algebra. She has been greatly undervalued. She was born in Germany in 1882, and with Noether's theorem, she showed scientists how to think about Nature in a new way, to build ideas such as those that lead to the search for the Higgs boson. She helped Einstein understand some of the issues in general relativity. Noether's theorem has been described as the cornerstone of modern subatomic physics. She achieved so much, yet for years she was banned from teaching at universities as she was a woman. And when she finally had a paid post, she was sacked from this under the nazism as she was Jewish. She died in exile in America in 1935 before her work was fully recognised.
</blockquote>

<script>
function hide() {
  var x = document.getElementById("transcription");
  if (x.style.display === "block") {
    x.style.display = "none";
  } else {
    x.style.display = "block";
  }
}
</script>
