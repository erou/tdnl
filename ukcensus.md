## Censuses

**Preliminary question:** why do governments take censuses of populations?

### Well-designed questions?

After reading this [article](https://www.theguardian.com/society/2023/nov/08/census-records-trans-population-in-england-and-wales-but-accuracy-is-doubted),
published in [The Guardian](https://en.wikipedia.org/wiki/The_Guardian), answer
the following questions.

+ What was the new question in the 2021 census in the UK?
+ According to the 2021 census, how many people identify as trans in the UK?
    + What proportion of the population does this number represent?
    + *Quick maths question:* what is the population in the UK then?
+ What was discovered concerning people identifying as trans, regarding their
    + main language?
    + Education?
    + Ethnicity?
+ Is it surprising?
+ According to critics, what was the problem with the question?
+ According to the Office for National Statistics, what could explain these
  results?
+ What does this article tell about question design?
+ What do **you** think about it? Can we trust the census' results?
